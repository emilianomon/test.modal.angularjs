# Vaga Javascript Developer #


### What is this repository for? ###

* This project aims to respond to the js challenge proposed by Celulardireto.

### Steps to Run ###

1. Install node ( [https://nodejs.org/en/download/](https://nodejs.org/en/download/) ).
2. Clone this project.
3. From terminal/cmd, navigate to the project's root folder.
4. Run `npm install`.
5. Run `gulp server`.
6. Access [http://localhost:8080/src/](http://localhost:8080/src/).

### Gulp Tasks ###

* `gulp server`
    * Runs server for the application.
* `gulp dev-server`
    * Runs server with automatic reload and sass compilation triggered as changes are made.
* `gulp inject-index`
    * Injects all css and js files in the index.
* `gulp sass-watch`
    * Triggers sass compilation whenever changes are made on these files.
* `gulp test`
    * Runs tests.
* `gulp inject-jasmine`
    * Injects src code in the SpecRunner.html.
* `gulp optimize`
    * Concatenates and unglifies js files, minifies css files for production, puts them in the "./src/dist" path and injects them in the index.html.


### Contact ###

* [emilianom.oliveira.n@gmail.com](mailto:emilianom.oliveira.n@gmail.com)
* +55 24 98862-7311