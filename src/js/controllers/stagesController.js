app.controller("stagesController", ["$scope", "$rootScope",
function ($scope, $rootScope) {

    angular.element("#SuccessAlert").fadeOut(1);

    $scope.subscribedAlert = false;
    $scope.model = {};
    $scope.model.currentStage = {
        step: "1/4",
        percentage: 25,
        style: {"width": "25%"}
    };

    $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
        if (toState.name == "app.platformSelection") {
            $scope.model.currentStage = {
                step: "1/4",
                percentage: 25,
                style: {"width": "25%"}
            };
        }
        else if (toState.name == "app.planSelection") {
            $scope.model.currentStage = {
                step: "2/4",
                percentage: 50,
                style: {"width": "50%"}
            };
        }
        else if (toState.name == "app.customerRegister") {
            $scope.model.currentStage = {
                step: "3/4",
                percentage: 75,
                style: {"width": "75%"}
            };
        }
        angular.element("#SuccessAlert").fadeOut(1000);
    });

    $scope.$on("customerSubscriptionSuccess", function () {
        $scope.model.currentStage = {
                step: "4/4",
                percentage: 100,
                style: {"width": "100%", "background-color": "#5cb85c"}
            };

        angular.element("#SuccessAlert").fadeIn(1000);
        console.log("");
    });

}]);