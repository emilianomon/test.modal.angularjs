app.controller("customerRegisterController", ["$scope", "$stateParams", "$rootScope", "$state", "$modal",
function ($scope, $stateParams, $rootScope, $state, $modal) {
    $scope.model = {};

    if (!$stateParams.platform.sku || !$stateParams.plan.sku)
        $state.go("app.platformSelection");

    $scope.model.platform = $stateParams.platform;
    $scope.model.plan = $stateParams.plan;

    $scope.model.customerInfo = {};

    $scope.finishSubscription = function (customerInfo) {

        $modal.open({
            templateUrl: "templates/modals/finish-subscription-modal.html",
            controller: "finishSubscriptionModalController",
            resolve: {
                subscription: function () {
                    return {
                        platform: $scope.model.platform,
                        plan: $scope.model.plan,
                        customerInfo: customerInfo
                    };
                }
            }
        }).result.then(
            function (result) {
                angular.element("#CheckoutButton").addClass("disabled");
                $rootScope.$broadcast("customerSubscriptionSuccess");
            },
            function (dismiss) {
                console.log("*** Modal closed by the user.");
            }
        );

    };

}]);