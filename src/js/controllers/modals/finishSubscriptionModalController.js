app.controller("finishSubscriptionModalController", ["$scope", "$modalInstance", "subscription",
function ($scope, $modalInstance, subscription) {

    $scope.model = {};
    $scope.model.platform = subscription.platform;
    $scope.model.plan = subscription.plan;
    $scope.model.customerInfo = subscription.customerInfo;

    $scope.closeModal = function () {
        $modalInstance.dismiss("modal_closed");
    };

    $scope.cancel = function () {
        $modalInstance.dismiss("canceled");
    };

    $scope.subscribe = function () {
        $modalInstance.close("subscribed");
    };  


}]);