app.controller("platformSelectionController", ["$scope", "platformService", "$state", "PLATFORM_NAMES",
function ($scope, platformService, $state, PLATFORM_NAMES) {
    
    $scope.model = {};
    $scope.model.PLATFORM_NAMES = PLATFORM_NAMES;
    $scope.model.platforms = [];

    $scope.getPlatforms = function () {
        platformService.getPlatforms().then(
            function (result) {
                console.log(result);
                $scope.model.platforms = result;
            },
            function (error) {
                console.log(error);
                alert("Error requesting platforms from the api.\nStatus: " + error.status);
            }
        )
    };

    $scope.selectPlatform = function (platform) {
        $state.go("app.planSelection", { platform: platform })
    };
}]);