app.controller("planSelectionController", ["$scope", "$stateParams", "$state", "planService", "$modal",
function ($scope, $stateParams, $state, planService, $modal) {
    $scope.model = {};
    $scope.model.platform = $stateParams.platform;
    $scope.model.plans = [];

    

    $scope.getPlans = function () {
        planService.getPlans($scope.model.platform.sku).then(
            function (result) {
                console.log(result);
                $scope.model.plans = result;
            },
            function (error) {
                console.log(error);
                alert("Error requesting plans from the api.\nStatus: " + error.status);
            }
        )
    }

    if (!$scope.model.platform.name)
        $state.go("app.platformSelection");
    else
        $scope.getPlans();

    $scope.selectPlan = function (plan) {
        $state.go("app.customerRegister", { platform: $scope.model.platform, plan: plan });
    };

}]);