app.directive("platformThumbnail", function () {
    return {
        restrict: "E",
        scope: {
            platform: "=",
            platformNames: "=",
            selectPlatform: "&"
        },
        templateUrl: "templates/directives/platform-thumbnail.html"
    }
});