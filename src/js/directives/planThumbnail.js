app.directive("planThumbnail", function () {
    return {
        restrict: "E",
        scope: {
            plan: "=",
            selectPlan: "&"
        },
        templateUrl: "templates/directives/plan-thumbnail.html"
    }
})