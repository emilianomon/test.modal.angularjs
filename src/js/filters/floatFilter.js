app.filter('float', function() {
    return function(stringNumber) {
      return parseFloat(stringNumber.replace(",", "."));
    };
});