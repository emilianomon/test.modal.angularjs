app.config(["$stateProvider", "$urlRouterProvider",
function ($stateProvider, $urlRouterProvider) {
    
    $stateProvider

        .state("app", {
            url: "/app",
            templateUrl: "templates/stages.html",
            controller: "stagesController"
        })
            .state("app.platformSelection", {
                parent: "app",
                url: "/platforms",
                templateUrl: "templates/platform-selection.html",
                controller: "platformSelectionController",
            })
            .state("app.planSelection", {
                parent: "app",
                url: "/platforms/plans",
                templateUrl: "templates/plan-selection.html",
                controller: "planSelectionController",
                params: { platform: {} }
            })
            .state("app.customerRegister", {
                parent: "app",
                url: "/platforms/plans/register",
                templateUrl: "templates/customer-register.html",
                controller: "customerRegisterController",
                params: { platform: {}, plan: {} }
            })
    
    $urlRouterProvider.otherwise("app/platforms")
}]);