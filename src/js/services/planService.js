app.service("planService", ["$http", "$q", "requestConfigFactory", "planFactory",
function ($http, $q, requestConfigFactory, planFactory) {

    this.getPlans = function (platformSku) {
        var deferrer = $q.defer();

        $http(requestConfigFactory.getPlansRequest(platformSku)).then(
            function (response) {

                var planList = [];
                for (i = 0; i < response.data.planos.length; i++) {
                    var device = undefined;
                    if (response.data.planos[i].aparelho) {
                        device = planFactory.device(
                            response.data.planos[i].aparelho.nome,
                            response.data.planos[i].aparelho.valor,
                            response.data.planos[i].aparelho.numeroParcelas,
                            response.data.planos[i].aparelho.valorParcela
                        );
                            
                    }

                    planList.push(planFactory.plan(
                        response.data.planos[i].sku,
                        response.data.planos[i].franquia,
                        response.data.planos[i].valor,
                        response.data.planos[i].ativo,
                        device
                    ));
                }

                console.log(planList);
                deferrer.resolve(planList);
            },
            function (error) {
                deferrer.reject(error);
            }
        );

        return deferrer.promise;
    }

}]);