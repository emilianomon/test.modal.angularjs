app.service("platformService", ["$http", "$q", "requestConfigFactory", "platformFactory",
function ($http, $q, requestConfigFactory, platformFactory) {

    this.getPlatforms = function () {
        var deferrer = $q.defer();

        $http(requestConfigFactory.getPlatformsRequest()).then(
            function (response) {
                console.log(response);

                var platformList = [];
                for (i = 0; i < response.data.plataformas.length; i++) {
                    platformList.push(platformFactory.platform(
                        response.data.plataformas[i].sku,
                        response.data.plataformas[i].nome,
                        response.data.plataformas[i].descricao
                    ));
                }

                deferrer.resolve(platformList);
                // deferrer.resolve(response.data);
            },
            function (error) {
                deferrer.reject(error);
            }
        );

        return deferrer.promise;
    };

}]);