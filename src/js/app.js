var app = angular.module("starter", ["ui.router", "ui.bootstrap", "angularMoment", "ui.bootstrap.datetimepicker", "ui.mask"]);

app.run(function (amMoment) {
    amMoment.changeLocale('pt-br');
});

