app.factory("planFactory", [
function () {

    var planFactory = {};

    planFactory.plan = function (sku, franchise, price, active, device) {
        return {
            sku: sku,
            franchise: franchise,
            price: price,
            active: active,
            device: device
        };
    };

    planFactory.device = function (name, price, installmentNumber, installmentPrice) {
        return {
            name: name,
            price: price,
            installmentNumber: installmentNumber,
            installmentPrice: installmentPrice
        };
    };

    return planFactory;

}])