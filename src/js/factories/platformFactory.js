app.factory("platformFactory", [
function () {

    var platformFactory = {};

    platformFactory.platform = function (sku, name, description) {
        return {
            sku: sku,
            name: name,
            description: description
        };
    };

    return platformFactory;

}]);