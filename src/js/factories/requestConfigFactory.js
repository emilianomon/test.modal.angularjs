app.factory("requestConfigFactory", [
function () {
    var requestConfigFactory = {};

    const baseUrl = "http://private-59658d-celulardireto2017.apiary-mock.com/";

    function buildRequestBase (method, path) {
        return {
            method: method,
            url: baseUrl + path,
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 2000
        }
    }

    requestConfigFactory.getPlatformsRequest = function () {
        return buildRequestBase("GET", "plataformas");
    };

    requestConfigFactory.getPlansRequest = function (platformSku) {
        return buildRequestBase("GET", "planos/" + platformSku);
    };

    return requestConfigFactory;
}]);