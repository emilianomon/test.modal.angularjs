describe("plan service", function () {
    var tabletPlansFromApi = {"planos": [ { "sku": "TI00001NA_NOVA_LINHA", "franquia": "1GB", "valor": "21,50", "ativo": true }, { "sku": "TI00002NA_NOVA_LINHA", "franquia": "2GB", "valor": "59,90", "ativo": true }, { "sku": "TI00003NA_NOVA_LINHA", "franquia": "4GB", "valor": "72,60", "ativo": true }, { "sku": "TI00004NA_NOVA_LINHA", "franquia": "6GB", "valor": "81,10", "ativo": false }, { "sku": "TI00005NA_NOVA_LINHA", "franquia": "10GB", "valor": "105,80", "ativo": true } ]};
    var computerPlansFromApi = {"planos": [ { "sku": "CI00001NA_NOVA_LINHA", "franquia": "1GB", "valor": "47,60", "aparelho": { "nome": "Modem", "valor": "299,00", "numeroParcelas": 5, "valorParcela": "59,80" }, "ativo": true }, { "sku": "CI00002NA_NOVA_LINHA", "franquia": "2GB", "valor": "59,90", "aparelho": { "nome": "Modem", "valor": "99,00", "numeroParcelas": 1, "valorParcela": false }, "ativo": true }, { "sku": "CI00003NA_NOVA_LINHA", "franquia": "4GB", "valor": "72,60", "aparelho": { "nome": "Modem", "valor": "99,00", "numeroParcelas": 1, "valorParcela": false }, "ativo": true } ]};
    var wifiPlansFromApi = {"planos": [ { "sku": "WI00001NA_NOVA_LINHA", "franquia": "1GB", "valor": "47,60", "aparelho": { "nome": "Roteador", "valor": "299,00", "numeroParcelas": 5, "valorParcela": "59,80" }, "ativo": true }, { "sku": "WI00002NA_NOVA_LINHA", "franquia": "2GB", "valor": "59,90", "aparelho": { "nome": "Roteador", "valor": "99,00", "numeroParcelas": 1, "valorParcela": false }, "ativo": true } ]};

    var tabletPlanModels = [ { "sku": "TI00001NA_NOVA_LINHA", "franchise": "1GB", "price": "21,50", "active": true, "device": undefined }, { "sku": "TI00002NA_NOVA_LINHA", "franchise": "2GB", "price": "59,90", "active": true, "device": undefined }, { "sku": "TI00003NA_NOVA_LINHA", "franchise": "4GB", "price": "72,60", "active": true, "device": undefined }, { "sku": "TI00004NA_NOVA_LINHA", "franchise": "6GB", "price": "81,10", "active": false, "device": undefined }, { "sku": "TI00005NA_NOVA_LINHA", "franchise": "10GB", "price": "105,80", "active": true, "device": undefined } ];
    var computerPlanModels = [ { "sku": "CI00001NA_NOVA_LINHA", "franchise": "1GB", "price": "47,60", "active": true, "device": { "name": "Modem", "price": "299,00", "installmentNumber": 5, "installmentPrice": "59,80" } }, { "sku": "CI00002NA_NOVA_LINHA", "franchise": "2GB", "price": "59,90", "active": true, "device": { "name": "Modem", "price": "99,00", "installmentNumber": 1, "installmentPrice": false } }, { "sku": "CI00003NA_NOVA_LINHA", "franchise": "4GB", "price": "72,60", "active": true, "device": { "name": "Modem", "price": "99,00", "installmentNumber": 1, "installmentPrice": false } } ];
    var wifiPlanModels = [ { "sku": "WI00001NA_NOVA_LINHA", "franchise": "1GB", "price": "47,60", "active": true, "device": { "name": "Roteador", "price": "299,00", "installmentNumber": 5, "installmentPrice": "59,80" } }, { "sku": "WI00002NA_NOVA_LINHA", "franchise": "2GB", "price": "59,90", "active": true, "device": { "name": "Roteador", "price": "99,00", "installmentNumber": 1, "installmentPrice": false } } ];

    var skuList = ["TBT01", "CPT02", "WF03"]; // Tablet, Computer and Wifi.

    var planService = {};
    var $httpBackend = {};

    beforeEach(module("starter"));
    beforeEach(inject(function (_planService_, _$httpBackend_) {
        planService = _planService_;
        $httpBackend = _$httpBackend_;
    }));

    it ("should return modeled plans for each platform", function () {
        
        var sku = skuList[1] // To test the request for different platforms, just change this index.
        var plansFromApi;
        var planModels;

        switch (sku) {
            case skuList[0]: plansFromApi = tabletPlansFromApi; planModels = tabletPlanModels; break;
            case skuList[1]: plansFromApi = computerPlansFromApi; planModels = computerPlanModels; break;
            case skuList[2]: plansFromApi = wifiPlansFromApi; planModels = wifiPlanModels; break;
            default: console.log("Chose a valid sku."); break;
        }

        function buildUrl (planSku) {
            return "http://private-59658d-celulardireto2017.apiary-mock.com/planos/" + planSku;
        }

        var result;
        $httpBackend.when("GET", buildUrl(sku)).
            respond(200, plansFromApi);

        planService.getPlans(sku).then(
            function (response) {
                result = response;
            },
            function (error) {
                result = error;
            }
        );

        $httpBackend.flush();

        expect(result).toEqual(planModels);
    })
});