describe("platform service", function () {

    var platformsFromApi = {"plataformas": [{"sku": "TBT01", "nome": "Tablet", "descricao": "Chip para navegar à vontade" }, { "sku": "CPT02", "nome": "Computador", "descricao": "Seu computador|Sempre conectado" }, { "sku": "WF03", "nome": "WI-FI", "descricao": "Internet WI-FI para|casa toda" } ] }
    var platformModels = [ { "sku": "TBT01", "name": "Tablet", "description": "Chip para navegar à vontade" }, { "sku": "CPT02", "name": "Computador", "description": "Seu computador|Sempre conectado" }, { "sku": "WF03", "name": "WI-FI", "description": "Internet WI-FI para|casa toda" } ]

    var platformService = {};
    var $httpBackend = {}; 

    beforeEach(module("starter"));
    beforeEach(inject(function(_platformService_, _$httpBackend_) {
        platformService = _platformService_;
        $httpBackend = _$httpBackend_;
    }));

    it("should return modeled platforms", function () {
        var result;

        $httpBackend.when("GET", "http://private-59658d-celulardireto2017.apiary-mock.com/plataformas")
            .respond(200, platformsFromApi);

        platformService.getPlatforms().then(
            function (response) {
                result = response;
            },
            function (error) {
                result = error;
            }
        );

        $httpBackend.flush();

        expect(result).toEqual(platformModels);

    })

});