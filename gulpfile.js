var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var inject = require("gulp-inject");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var csso = require("gulp-csso");
var concatCss = require("gulp-concat-css");
var karmaServer = require("karma").Server;
var connect = require("gulp-connect");

// Compiles sass files.
gulp.task("sass-compile", function () {
    return gulp.src("./src/assets/css/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer({browsers: ["last 2 versions", "> 5%"]}))
        .pipe(gulp.dest("./src/assets/css"));
});

gulp.task("server", function () {
    connect.server({
        livereload: false,
        port: 8080,
        host: "localhost",
        root: ["."]
    });
});

// If it stops autoreloading, clear the cache and reload the page.
gulp.task("dev-server", function () {
    connect.server({
        livereload: true,
        port: 8080,
        host: "localhost",
        root: ["."]
    });
    gulp.watch(["./src/assets/css/*.scss"], ["sass-compile", "server-reload"]); // sass
    gulp.watch(["./src/js/*.js", "./src/js/**/*.js", "./src/js/**/**/*.js"], ["server-reload"]); // js
    gulp.watch(["./src/*.html", "./src/templates/*.html", "./src/templates/**/*.html"], ["server-reload"]); // html
});

gulp.task("server-reload", function () {
    gulp.src(".").pipe(connect.reload());
});

gulp.task("sass-watch", function () {
    gulp.watch(["./src/assets/css/*.scss"], ["sass-compile"]);
});

gulp.task("test", function (done) {
    new karmaServer({
        configFile: __dirname + "/karma.conf.js",
        singleRun: true
    }, done).start();
});

// Injects the source file at the index file for development.
gulp.task("inject-index", function () {
    var target = gulp.src("./src/index.html");
    var sources = gulp.src(getNodeModulesSources().concat(["./src/js/**/*.js", "./src/**/**/.js", "./src/assets/css/*.css"]));

    target
        .pipe(inject(sources))
        .pipe(gulp.dest("./src"));
});

gulp.task("inject-jasmine", function () {
    var target = gulp.src("./tests/SpecRunner.html");
    var sources = gulp.src(getNodeModulesSources()
                            .concat(getTestingNodeModulesSources())
                            .concat(["./src/js/app.js", "./src/js/routes.js", "./src/js/**", "./tests/spec/**.spec.js"]));
    target
        .pipe(inject(sources))
        .pipe(gulp.dest("./tests"));
});

// Optimizes the source files and injects them in the index file for production.
gulp.task("optimize", function () {

    var target = gulp.src("./src/index.html");
    var nodeModulesSources = gulp.src(getNodeModulesSources());
    var jsSources = gulp.src(["./src/js/*.js", "./src/js/**/*.js", "./src/**/**/.js"]);
    var cssSources = gulp.src(["./src/assets/css/*.css"]);

    nodeModulesSources
        .pipe(concat("node_modules.js"))
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest("./src/dist"));
    jsSources
        .pipe(concat("app.js"))
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest("./src/dist"));
    cssSources
        .pipe(concatCss("bundle.css"))
        .pipe(csso())
        .pipe(gulp.dest("./src/dist"));

    var distSources = gulp.src(["./src/dist/node_modules.js", "./src/dist/*.js", "./src/dist/*.css"]);

    target
        .pipe(inject(distSources))
        .pipe(gulp.dest("./src"));
});


// Insert all JS files located within node modules in the array bellow.
function getNodeModulesSources () {
    return [
        "./node_modules/jquery/dist/jquery.js",
        "./node_modules/moment/moment.js",
        "./node_modules/moment/locale/pt-br.js",
        "./node_modules/angular/angular.js",
        "./node_modules/angular-moment/angular-moment.js",
        "./node_modules/angular-ui-router/release/angular-ui-router.js",
        "./node_modules/bootstrap/dist/js/bootstrap.min.js",
        "./node_modules/angular-i18n/angular-locale_pt-br.js",
        "./node_modules/angular-bootstrap/ui-bootstrap-tpls.min.js",
        "./node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.js",
        "./node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js",
        "./node_modules/angular-ui-mask/dist/mask.min.js"
    ];
}

// Insert all testing framework files in the array bellow. 
function getTestingNodeModulesSources() {
    return [
        "./node_modules/jasmine-core/lib/jasmine-core/jasmine.js",  
        "./node_modules/jasmine-core/lib/jasmine-core/jasmine-html.js",
        "./node_modules/jasmine-core/lib/jasmine-core/boot.js",
        "./node_modules/angular-mocks/angular-mocks.js"
    ];
}